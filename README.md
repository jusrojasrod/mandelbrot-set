# Mandelbrot Set

Code to visualize the Mandelbrot set in the Complex plane.

![Mandelbrot_V1](/uploads/c183b01fb1349bbbf623a3fc07ad3dd1/Mandelbrot_V1.png)
